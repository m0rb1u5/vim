" All system-wide defaults are set in $VIMRUNTIME/debian.vim and sourced by
" the call to :runtime you can find below.  If you wish to change any of those
" settings, you should do it in this file (/etc/vim/vimrc), since debian.vim
" will be overwritten everytime an upgrade of the vim packages is performed.
" It is recommended to make changes after sourcing debian.vim since it alters
" the value of the 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
"set compatible

" Vim5 and later versions support syntax highlighting. Uncommenting the next
" line enables syntax highlighting by default.
syntax on

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
set background=dark

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has('autocmd')
	au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
if has('autocmd')
	filetype plugin indent on
endif

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
"set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
set incsearch		" Incremental search
"set autowrite		" Automatically save before commands like :next and :make
"set hidden		" Hide buffers when they are abandoned
set mouse=a		" Enable mouse usage (all modes)

" Source a global configuration file if available
if filereadable('/etc/vim/vimrc.local')
	source /etc/vim/vimrc.local
endif
set expandtab
retab
set autoindent
set hlsearch
set paste
set ignorecase
set number
set tabstop=3
set shiftwidth=3
set cindent
set smartindent
set autoindent
set smarttab
colorscheme default

set laststatus=2
set noshowmode

:command! -complete=file -nargs=1 Rpdf :r !pdftotext -nopgbrk <q-args> -
:command! -complete=file -nargs=1 Rpdf :r !pdftotext -nopgbrk <q-args> - |fmt -csw78

let g:vim_markdown_preview_github=1
let g:vim_markdown_preview_use_xdg_open=0
let g:vim_markdown_preview_browser='Google Chrome'
let g:vmp_osname='unix'
let g:indent_guides_enable_on_vim_startup=1
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=233
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=234
if !&diff
	autocmd vimenter * NERDTree
	autocmd vimenter * Tagbar
	autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
	let g:NERDTreeDirArrowExpandable = '▸'
	let g:NERDTreeDirArrowCollapsible = '▾'
	let g:NERDTreeIndicatorMapCustom = {'Modified': '✹', 'Staged': '✚', 'Untracked': '✭', 'Renamed': '➜', 'Unmerged': '═', 'Deleted': '✖', 'Dirty': '✗', 'Clean': '✔︎', 'Ignored': '☒', 'Unknown': '?'}
	set updatetime=100

	let g:ale_linters = {'javascript': ['eslint'], 'python': ['flake8'], 'html': ['alex', 'htmlhint', 'proselint', 'write-good'], 'tex': ['alex', 'proselint', 'write-good'], 'markdown': ['alex', 'proselint', 'write-good'], 'help': ['alex', 'proselint', 'write-good'], 'xhtml': ['alex', 'proselint', 'write-good'], 'json': ['jsonlint'], 'sql': ['sqlint'], 'css': ['csslint', 'stylelint'], 'vim': ['vint'], 'r': ['lintr'], 'typescript': ['tslint'], 'java': ['javac']}
	let g:ale_fixers = {'javascript': ['eslint']}
	let g:ale_fix_on_save = 0
	let g:ale_completion_enabled = 1
	let g:ycm_global_ycm_extra_conf = '/etc/vim/.ycm_extra_conf.py'
	let g:ale_java_checkstyle_options = '/etc/vim/.google_checks.xml'
	let g:tagbar_type_typescript = {'ctagsbin' : 'tstags', 'ctagsargs' : '-f-', 'kinds': ['e:enums:0:1', 'f:function:0:1', 't:typealias:0:1',	'M:Module:0:1', 'I:import:0:1', 'i:interface:0:1',	'C:class:0:1',	'm:method:0:1', 'p:property:0:1', 'v:variable:0:1', 'c:const:0:1', ], 'sort' : 0}
endif

if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'itchyny/lightline.vim'
Plug 'JamshedVesuna/vim-markdown-preview'
Plug 'Valloric/YouCompleteMe'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'leafgarland/typescript-vim'
Plug 'ap/vim-css-color'
Plug 'artur-shaik/vim-javacomplete2'
if !&diff
	Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
	Plug 'Xuyuanp/nerdtree-git-plugin'
	Plug 'airblade/vim-gitgutter'
	Plug 'w0rp/ale'
	Plug 'ashisha/image.vim'
	Plug 'majutsushi/tagbar'
	Plug 'chrisbra/csv.vim'
endif

call plug#end()
